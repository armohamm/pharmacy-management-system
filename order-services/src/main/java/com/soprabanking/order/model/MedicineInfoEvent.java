package com.soprabanking.order.model;

public class MedicineInfoEvent {

	private int medicineCount; 
	private boolean medicineAvailablity;

	public MedicineInfoEvent() {
	}

	public MedicineInfoEvent(int medicineCount, boolean medicineAvailablity) {
		super();
		this.medicineCount = medicineCount;
		this.medicineAvailablity = medicineAvailablity;
	}

	public int getMedicineCount() {
		return medicineCount;
	}

	public void setMedicineCount(int medicineCount) {
		this.medicineCount = medicineCount;
	}

	public boolean isMedicineAvailablity() {
		return medicineAvailablity;
	}

	public void setMedicineAvailablity(boolean medicineAvailablity) {
		this.medicineAvailablity = medicineAvailablity;
	}

	@Override
	public String toString() {
		return "MedicineInfoEvent [medicineCount=" + medicineCount + ", medicineAvailablity=" + medicineAvailablity
				+ "]";
	}

}
