package com.soprabanking.order.util;

public interface GlobalConstants {
	
    public static final String ORDERS_SEQUENCE = "orders_sequence";
    public static final String DATABASE_SEQ_COLUMN_SEQ = "seq";
    public static final String DATABASE_SEQ_COLUMN_ID = "_id";
    public static final String DEFAULT_ORDER_STATUS = "PENDING";
    public static final String URL_UPDATE_MEDICINE = "http://medicine-services/pharmacy/medicine/editMedicine/";
    public static final String URL_GET_MEDICINE_BY_ID = "http://medicine-services/pharmacy/medicine/getMedicine/";
 
}
