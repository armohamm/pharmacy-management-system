package com.soprabanking.order.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.soprabanking.order.model.Order;

import reactor.core.publisher.Flux;

@Repository
public interface OrderRepo extends ReactiveMongoRepository<Order, Integer> {
	@Query("{ 'userName' : ?0 }")
	Flux<Order> findOrdersByUserName(String userName);
}
