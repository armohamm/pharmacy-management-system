package com.soprabanking.medicine.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.soprabanking.medicine.model.Medicine;

@Repository
public interface MedicineRepo extends ReactiveMongoRepository<Medicine, Integer> {

}
